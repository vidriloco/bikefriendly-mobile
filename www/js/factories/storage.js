app.factory('storeService', function(){
    function StoreService(){
        this.bikefriendlyItems = [];
        this.bikeshopItems = [];
        
        this.findBikefriendlyById = function(id) {
          for(var idx in this.bikefriendlyItems) {
            if(this.bikefriendlyItems[idx].id === parseInt(id))
              return this.bikefriendlyItems[idx];
          }
        }
        
        this.findBikeshopById = function(id) {
          for(var idx in this.bikeshopItems) {
            if(this.bikeshopItems[idx].id === parseInt(id))
              return this.bikeshopItems[idx];
          }
        }
    }
    
    StoreService.prototype.updateBikefriendlyItemsList = function(list){
        this.bikefriendlyItems = list;
    }

    StoreService.prototype.updateBikeshopItemsList = function(list){
        this.bikeshopItems = list;
    }

    StoreService.prototype.findBikeshopById = function(id){
        return this.findBikeshopById(id);
    }
    
    StoreService.prototype.findBikefriendlyById = function(id){
      return this.findBikefriendlyById(id);
    }
    
    StoreService.prototype.allBikefriendlyItems = function(id){
        return this.bikefriendlyItems;
    }
    
    StoreService.prototype.allBikeshopItems = function(id){
        return this.bikeshopItems;
    }

    return new StoreService();
});