app.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
  if (ionic.Platform.isAndroid()) {
    $ionicConfigProvider.scrolling.jsScrolling(false);
  }
  
  $stateProvider
  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'MainController'
  })
  .state('app.bikefriendlies', {
    url: '/bikefriendlies',
    views: {
      'menuContent': {
        templateUrl: 'templates/bikefriendlies-list.html',
        controller: 'BikefriendliesController'
      }
    }
  })
  .state('app.bikefriendly-details', {
    url: '/bikefriendly-details/{id}',
    views: {
      'menuContent': {
        templateUrl: 'templates/bikefriendly-details.html',
        controller: 'BikefriendliesController'
      }
    }
  })
  .state('app.bikeshops', {
    url: '/bikeshops',
    views: {
      'menuContent': {
        templateUrl: 'templates/bikeshops-list.html',
        controller: 'BikeshopsController'
      }
    }
  })
  .state('app.bikeshop-details', {
    url: '/bikeshop-details/{id}',
    views: {
      'menuContent': {
        templateUrl: 'templates/bikeshop-details.html',
        controller: 'BikeshopsController'
      }
    }
  })
  .state('app.bikeparkings', {
    url: '/bikeparkings',
    views: {
      'menuContent': {
        templateUrl: 'templates/bikeparkings-list.html',
        controller: 'BikeparkingsController'
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/bikefriendlies');
});