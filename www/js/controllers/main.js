app.controller('MainController', function($scope, $ionicSideMenuDelegate, $location) {
    
    $scope.bikefriendlyVenuesShouldAppear = function() {
        return $location.path() == "/app/bikefriendlies";
    }
    
    $scope.bikeshopsShouldAppear = function() {
        return $location.path() == "/app/bikeshops";
    }
    
    $scope.bikeparkingsShouldAppear = function() {
        return $location.path() == "/app/bikeparkings";
    }
});