app.controller('BikefriendliesController', function($scope, $http, ApiSettings, storeService, $stateParams, $ionicScrollDelegate, $rootScope, $state, $sce, $cordovaInAppBrowser, $cordovaGeolocation) {

    $rootScope.slideHeader = false;
    $rootScope.slideHeaderPrevious = 0;
    
    $scope.selectedBikefriendlyId = $stateParams.id;
    $scope.selectedbikefriendlyItem = null;
    
    $scope.fetching = false;
    
    var page = 0;
    var lastLat = null;
    var lastLng = null;
    
    var fetchMoreBikefriendlyItems = function(callback_function, error_callback) {
      
      var makeRequest = function() {
        $scope.fetching = true;
        var url = ApiSettings.url.concat("/venues/bikefriendly/").concat(lastLng).concat('/').concat(lastLat).concat('/').concat(page).concat('.json');
        var encodedURL = CryptoJS.SHA1(ApiSettings.key.concat(':').concat(url));
        
        $http.get(url, {
          headers: {
            apiKey: encodedURL,
            applicationId: ApiSettings.applicationId
          }
        }).then(function(response) {
          storeService.updateBikefriendlyItemsList(response.data.venues);
          $scope.fetching = false;
          if(callback_function)
            callback_function();
         }, function(err) {
           if(error_callback)
             error_callback();
           else
             alert("Bikefriendly App necesita una conexión a internet para funcionar correctamente");
         });
      }
      
      if(lastLat == null && lastLng == null) {
        $scope.fetching = true;
        setTimeout(function(){
          updateLocation(makeRequest);
        }, 500);
      } else
        makeRequest();
    }
    
    var updateLocation = function(callback_function, error_callback) {
      var posOptions = {timeout: 10000, enableHighAccuracy: false};
      $cordovaGeolocation.getCurrentPosition(posOptions)
        .then(function (position) {
          lastLat = position.coords.latitude;
          lastLng = position.coords.longitude;
          if(callback_function)
            callback_function();
        }, function(err) {
          if(error_callback)
            error_callback();
      });
    }
    
    $scope.selectedItem = function() {
      if($scope.selectedbikefriendlyItem == null)
        $scope.selectedbikefriendlyItem = storeService.findBikefriendlyById($scope.selectedBikefriendlyId);
      
      return $scope.selectedbikefriendlyItem;
    }
    
    $scope.bikefriendlyItems = function() {
      return storeService.allBikefriendlyItems();
    }
    
    $scope.$on('$ionicView.beforeEnter', function() {
      var loadComponentsForDetailView = function() {
        if(typeof $scope.selectedBikefriendlyId !== "undefined") {        
          var iconURL = 'img/bikefriendly-map-icon-'+$scope.selectedItem().rank+'.png';
          $rootScope.loadMapAtSelectedItemCoordinate($scope.selectedItem().coordinates, iconURL);
        }
      }
      
      if(storeService.allBikefriendlyItems().length == 0)
        fetchMoreBikefriendlyItems(loadComponentsForDetailView);
      else
        loadComponentsForDetailView();

    });
    
    $scope.iconForSelectedItem = function() {
      if($scope.selectedItem() && $scope.selectedItem().logo_pic)
        return $scope.selectedItem().logo_pic;
      else
        return "img/bikefriendly-place-icon-selected.png";
    }
    
    $scope.sanitizeText = function(text) {
        return $sce.trustAsHtml(text);
    };

    $scope.fetchMoreBikefriendlyVenues = function() {
      $scope.fetching = true;
      var resetPullerUIComponentStatus = function() {
        $scope.$broadcast('scroll.refreshComplete');
      }
      
      updateLocation(function() {
        fetchMoreBikefriendlyItems(resetPullerUIComponentStatus, resetPullerUIComponentStatus);
      }, resetPullerUIComponentStatus);  
    };
    
    $scope.showBikefriendlyDetailsFor = function(item) {
      $state.go('app.bikefriendly-details', { id: item.id });
    };
    
});