app.controller('BikeparkingsController', function($http, $cordovaGeolocation, ApiSettings, $scope, $ionicScrollDelegate, $rootScope, $sce, $ionicPopup) {
    $rootScope.slideHeader = false;
    $rootScope.slideHeaderPrevious = 0;
    
    $scope.currentItem = null;
    
    $scope.items = [];
    $scope.fetching = false;
    
    var page = 0;
    var lastLat = null;
    var lastLng = null;
    
    var fetchMoreBikeparkingItems = function(callback_function, error_callback) {
      
      var makeRequest = function() {
        $scope.fetching = true;
        var url = ApiSettings.url.concat("/parkings/").concat(lastLng).concat('/').concat(lastLat).concat('/').concat(page).concat('.json');
        var encodedURL = CryptoJS.SHA1(ApiSettings.key.concat(':').concat(url));
        
        $http.get(url, {
          headers: {
            apiKey: encodedURL,
            applicationId: ApiSettings.applicationId
          }
        }).then(function(response) {
          $scope.items = response.data.parkings;
          $scope.fetching = false;
          if(callback_function)
            callback_function();
         }, function(err) {
           if(error_callback)
             error_callback();
           else
             alert("Bikefriendly App necesita una conexión a internet para funcionar correctamente");
         });
      }
      
      if(lastLat == null && lastLng == null) {
        $scope.fetching = true;
        setTimeout(function(){
          updateLocation(makeRequest);
        }, 500);
      } else
        makeRequest();
    }
    
    $scope.$on('$ionicView.beforeEnter', function() {     
      if($scope.items.length == 0)
        fetchMoreBikeparkingItems();
    });
    
    var updateLocation = function(callback_function, error_callback) {
      var posOptions = {timeout: 10000, enableHighAccuracy: false};
      $cordovaGeolocation.getCurrentPosition(posOptions)
        .then(function (position) {
          lastLat = position.coords.latitude;
          lastLng = position.coords.longitude;
          if(callback_function)
            callback_function();
        }, function(err) {
          if(error_callback)
            error_callback();
      });
    }
    
    $scope.fetchMoreBikeparkings = function() {
      $scope.fetching = true;
      var resetPullerUIComponentStatus = function() {
        $scope.$broadcast('scroll.refreshComplete');
      }
      
      updateLocation(function() {
        fetchMoreBikeparkingItems(resetPullerUIComponentStatus, resetPullerUIComponentStatus);
      }, resetPullerUIComponentStatus);  
    };
    
    $scope.showBikeparkingDetailsFor = function(item) {
      $scope.data = {}
      
      $scope.currentItem = item;
      
      // An elaborate, custom popup
      var myPopup = $ionicPopup.show({
        templateUrl: 'templates/bikeparking-details.html',
        title: 'Detalles de bici-estacionamiento',
        subTitle: $scope.bikeParkingTitleFor($scope.currentItem),
        scope: $scope,
        cssClass: 'bikeparking-dialog',
        buttons: [
          { text: 'Cerrar' },
          {
            text: 'Ver en mapa',
            type: 'button-balanced',
            onTap: function(e) {
              $scope.openMap($scope.currentItem.coordinates);
            }
          }
        ]
      });
     };
    
    $scope.bikeParkingTitleFor = function(parkingItem) {
      return parkingItem.kind_humanized;
    };
    
    $scope.bikeParkingImageFor = function(parkingItem) {
      // 1 => :rack, 2 => :other, 3 => :private, 4 => :cycleport
      if(parkingItem.kind == 1)
        return "img/parking-rack-icon.jpg";
      else if(parkingItem.kind == 2)
        return "img/parking-other-icon.jpg";
      else if(parkingItem.kind == 3)
        return "img/parking-venue-icon.jpg";
      else
        return parkingItem.front_image;
    };
    
    $scope.sanitizeText = function(text) {
        return $sce.trustAsHtml(text);
    };
    
    $scope.sanitizeItemSource = function(parkingItem) {
        return $sce.trustAsHtml(parkingItem.source);
    };
});