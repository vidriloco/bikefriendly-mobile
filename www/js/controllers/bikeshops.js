app.controller('BikeshopsController', function($scope, $stateParams, ApiSettings, storeService, $http, $ionicScrollDelegate, $rootScope, $ionicModal, $state, $sce, $cordovaGeolocation) {
    $rootScope.slideHeader = false;
    $rootScope.slideHeaderPrevious = 0;
    
    $scope.selectedBikeshopId = $stateParams.id;
    $scope.selectedBikeshopItem = null;
    
    $scope.fetching = false;
    
    var page = 0;
    var lastLat = null;
    var lastLng = null;
    
    var fetchMoreBikeshopItems = function(callback_function, error_callback) {
      var makeRequest = function() {
        $scope.fetching = true;
        var url = ApiSettings.url.concat("/venues/bikeshops/").concat(lastLng).concat('/').concat(lastLat).concat('/').concat(page).concat('.json');
        var encodedURL = CryptoJS.SHA1(ApiSettings.key.concat(':').concat(url));
        
        $http.get(url, {
          headers: {
            apiKey: encodedURL,
            applicationId: ApiSettings.applicationId
          }
        }).then(function(response) {
          storeService.updateBikeshopItemsList(response.data.venues);
          $scope.fetching = false;
          if(callback_function)
            callback_function();
         }, function(err) {
           if(error_callback)
             error_callback();
           else
             alert("Bikefriendly App necesita una conexión a internet para funcionar correctamente");
         });
      }
      
      if(lastLat == null && lastLng == null) {
        $scope.fetching = true;
        setTimeout(function(){
          updateLocation(makeRequest);
        }, 500);
      } else
        makeRequest();
    }
    
    var updateLocation = function(callback_function, error_callback) {
      var posOptions = {timeout: 10000, enableHighAccuracy: false};
      $cordovaGeolocation.getCurrentPosition(posOptions)
        .then(function (position) {
          lastLat = position.coords.latitude;
          lastLng = position.coords.longitude;
          if(callback_function)
            callback_function();
        }, function(err) {
          if(error_callback)
            error_callback();
      });
    }
    
    $scope.selectedItem = function() {
      if($scope.selectedBikeshopItem == null)
        $scope.selectedBikeshopItem = storeService.findBikeshopById($scope.selectedBikeshopId);
      
      return $scope.selectedBikeshopItem;
    }
    
    $scope.bikehopItems = function() {
      return storeService.allBikeshopItems();
    }

     $scope.$on('$ionicView.beforeEnter', function() {
       var loadComponentsForDetailView = function() {
         if(typeof $scope.selectedBikeshopId !== "undefined") {       
           var iconURL = 'img/bikefriendly-map-icon-'+$scope.selectedItem().rank+'.png';
           $rootScope.loadMapAtSelectedItemCoordinate($scope.selectedItem().coordinates, iconURL);
         }
       }
      
       if(storeService.allBikeshopItems().length == 0)
         fetchMoreBikeshopItems(loadComponentsForDetailView);
       else
         loadComponentsForDetailView();
     });
    
     $scope.sanitizeText = function(text) {
         return $sce.trustAsHtml(text);
     };
    
     $scope.fetchMoreBikeshopVenues = function() {
       $scope.fetching = true;
       var resetPullerUIComponentStatus = function() {
         $scope.$broadcast('scroll.refreshComplete');
       }
       
       updateLocation(function() {
         fetchMoreBikeshopItems(resetPullerUIComponentStatus, resetPullerUIComponentStatus);
       }, resetPullerUIComponentStatus);  
     };
    
    $scope.showBikeShopDetailsFor = function(item) {
      $state.go('app.bikeshop-details', { id: item.id });
    };
    
    $scope.iconForSelectedItem = function() {
      if($scope.selectedItem() && $scope.selectedItem().logo_pic)
        return $scope.selectedItem().logo_pic;
      else
        return "img/bikeshop-icon-selected.png";
    }
});