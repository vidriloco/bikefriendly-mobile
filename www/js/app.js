// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js

var app = angular.module('bikefriendlyapp', ['ionic', 'ngSanitize', 'ngCordova', 'ngIOS9UIWebViewPatch'])
.run(function($ionicPlatform, $rootScope) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
  
  $rootScope.loadMapAtSelectedItemCoordinate = function(coordinates, iconURL) {
    var myLatlng = new google.maps.LatLng(coordinates.lat, coordinates.lng);
    var mapRecenter = function(offsetx, offsety) {
      var point1 = map.getProjection().fromLatLngToPoint(map.getCenter());
      var point2 = new google.maps.Point(
          ( (typeof(offsetx) == 'number' ? offsetx : 0) / Math.pow(2, map.getZoom()) ) || 0,
          ( (typeof(offsety) == 'number' ? offsety : 0) / Math.pow(2, map.getZoom()) ) || 0
      );  
      map.setCenter(map.getProjection().fromPointToLatLng(new google.maps.Point(
          point1.x - point2.x,
          point1.y + point2.y
      )));
    }
    
    var map = new google.maps.Map(document.getElementById("map"), {
      center: myLatlng,
      zoom: 16,
      mapTypeControl: false,
      zoomControl: false,
      streetViewControl: false,
      mapTypeId: google.maps.MapTypeId.ROADMAP, 
      scrollwheel: false, 
      disableDoubleClickZoom: true,
      draggable: false
    });
              
    var image = {
        url: iconURL,
        scaledSize: new google.maps.Size(70, 70)
    };
        
    var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      icon: image
    });     
    
    google.maps.event.addListenerOnce(map, 'idle', function(){
      mapRecenter(0, -30);
    }); 
  }
  
  $rootScope.dialNumber = function(number) {
    window.plugins.phoneDialer.dial(number);
    return false;
  };
  
  $rootScope.openTwitterAccount = function(account) {
    var scheme;

    if(ionic.Platform.isIOS()) {
        scheme = 'twitter://';
        appAvailability.check(
          scheme,
          function() {
            window.open('twitter://user?screen_name='.concat(account), '_system', 'location=no');
          },
          function() {
            window.open('https://twitter.com/'.concat(account), '_system', 'location=no');
          }
        );
    } else if(ionic.Platform.isAndroid()) {
        window.open('https://twitter.com/'.concat(account), '_system', 'location=no');   
    }
  }
  
  $rootScope.openFacebookAccountURL = function(accountURL) {
    window.open(accountURL, '_system', 'location=no');
  }
  
  $rootScope.openMap = function(coordinates) {
    
    var coords = coordinates.lat.toString().concat(", ").concat(coordinates.lng.toString());
    
    if(ionic.Platform.isIOS()) {
      appAvailability.check(
          'comgooglemaps://',
          function() {
            window.open("comgooglemaps://?q=".concat(coords).concat('&zoom=14&views=traffic'), '_system');
          },
          function() {
            // No google maps found, then open apple maps :S
            appAvailability.check(
              'maps://',
              function() {
                window.open("maps://maps.apple.com/?q=".concat(coords), '_system'); 
              },
              function() {
               
              }
            );
          }
        );
    } else if(ionic.Platform.isAndroid()) {
        appAvailability.check(
            'com.google.maps',
            function() {
              window.open("comgooglemaps://?q=".concat(coords).concat('&zoom=14&views=traffic'), '_system');
            },
            function() {
              window.open("http://maps.google.com/maps?z=12&t=m&q=loc:".concat(coordinates.lat.toString()).concat("+").concat(coordinates.lng.toString()), '_system');
            }
        );
    }

  }
  
});
