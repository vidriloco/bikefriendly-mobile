/*app.constant('ApiSettings', {
  url: 'http://192.168.1.66:3000/api'
  url: 'http://localhost:8100/api'
})*/

/* Add on ionic.project file

,
  "proxies": [
      {
        "path": "/api",
        "proxyUrl": "http://127.0.0.1:3000/api"
      }
    ]

*/

// Settings for production environment
app.constant('ApiSettings', {
  url: 'http://bikefriendlyapp.com/api',
  key: 'fd28dd99a6b33e3d916e',
  applicationId: 'bikefriendly-app'
});
